function myOrientResizeFunction(){
	var $width = $(this).width(),
		$allviews = $('.content-filter__list--item[class*="view--"]'),
		$active = 'active';

	if( $('.main-content__wrap').hasClass('post-view--list') || $('.main-content__wrap').hasClass('post-view--grid') ) {
		if( $width > 959 || $width < 600 ) {
			$allviews.removeClass($active);
			$('.content-filter__list--item[class*="view--grid"]').addClass($active);
			$('.main-content__wrap').removeClass('post-view--list').addClass('post-view--grid');
		} else if( $width < 960 ) {
			$allviews.removeClass($active);
			$('.content-filter__list--item[class*="view--list"]').addClass($active);
			$('.main-content__wrap').removeClass('post-view--grid').addClass('post-view--list');
		}
	}
}

var $stickyClass = 'sticky',
	$stickyElement = '.global-header',
	$stickyNavTop = $($stickyElement).offset().top;

var stickyNav = function( elements ){
	var $scrollTop = $(window).scrollTop();

	if ($scrollTop > $stickyNavTop/* && $(window).width() > 960*/ ) {
		$('body').addClass($stickyClass).css({ 'padding-top' : $($stickyElement).height() });
	} else {
		$('body').removeClass($stickyClass).css({ 'padding-top' : '' });
	}
};

(function($){

	$(document).ready(function() {
		myOrientResizeFunction();

		stickyNav($stickyElement);
	});

	$(window).on({
		'resize' : function(){
			myOrientResizeFunction();
		},
		'scroll' : function() {
			stickyNav($stickyElement);
		}
	});

	if(window.DeviceOrientationEvent) {
		window.addEventListener('orientationchange', myOrientResizeFunction, false);
	}

	$('input').keyup(function(){
		if( $(this).val() !== "" ){
			$(this).addClass("not-empty");
		} else {
			$(this).removeClass("not-empty");
		}
	}).placeholder();

	$(".post").fitVids();

	$('.content-filter__list--item[class*="view--"]').on('click', function(){
		var $this = $(this),
			$allviews = $('.content-filter__list--item[class*="view--"]'),
			$active = 'active';

		$allviews.removeClass($active);
		$this.addClass($active);

		$('.main-content__wrap').removeClass('post-view--grid post-view--list').addClass('post-view--' + $this.data('view'));
	});

	$('.content-filter__list--item[data-filter]').on('click', function(){
		var $this = $(this),
			$allviews = $('.content-filter__list--item[data-filter]'),
			$active = 'active';

		$allviews.removeClass($active);
		$this.addClass($active);

		$('.main-content__wrap .post__parent').hide();

		if( $this.data('filter') == 'all' ) {
			$('.main-content__wrap .post__parent').fadeIn('slow');
		} else {
			$('.main-content__wrap .post__parent.post-type--' + $this.data('filter') ).fadeIn('slow');
		}
	});

	$('.on-click--show').on('click', function(){
		var $this = $(this),
			$child = $this.next('.hidden'),
			$active = 'active';

		if( $child.hasClass($active) ) {
			$child.removeClass($active).slideUp();
		} else {
			$child.addClass($active).slideDown();
		}
	});

	$('.on-click--swap-text').on('click', function(){
		var $this = $(this),
			$openText = $(this).data('open-text'),
			$closeText = $(this).data('close-text'),
			$active = 'active';

		if( $this.hasClass($active) ) {
			$this.text($openText).removeClass($active);
		} else {
			$this.text($closeText).addClass($active);
		}
	});

	$('.on-click--popup').each(function(){
		$('#' + $(this).data('popup-id')).popup({
			opacity: 0.8,
			transition: 'all 0.3s',
			vertical: 'top',
			opentransitionend: function() {
				$('.global-header, .hero, .content-filter, .main-content__wrap').addClass('effect--blur');
			},
			onclose: function() {
				$('.global-header, .hero, .content-filter, .main-content__wrap').removeClass('effect--blur');
			},
		});
	});

})(jQuery);